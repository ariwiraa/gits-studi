package main

import (
	"errors"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"unicode"
)

func CheckNumber(example string) string {
	var reverseString string
	_, err := strconv.Atoi(example)
	if err != nil {
		reg, err := regexp.Compile("[^a-zA-Z]+")
		if err != nil {
			log.Fatal(err)
		}
		processedString := reg.ReplaceAllString(example, "")
		reverseString = Reverse(processedString)
	}
	return reverseString
}
func capFirstChar(s string) string {
	for index, value := range s {
		return string(unicode.ToUpper(value)) + s[index+1:]
	}
	return ""
}

func Reverse(s string) (result string) {
	for _, v := range s {
		result = string(v) + result
	}
	return
}

func CheckString(word interface{}) (interface{}, error) {
	switch value := word.(type) {
	case string:
		return value, nil
	default:
		return 0, errors.New("Harus Terdapat String")
	}

}

func main() {
	//Nomor 1A
	example := CheckNumber("123oll4eH5")
	fmt.Print(example)

	//Nomor 1B
	word, err := CheckString(1)
	if err == nil {
		fmt.Println(word)
	} else {
		fmt.Println("\nError", err.Error())
	}

	//Nomor 1C
	result := Reverse("Red")
	fmt.Println(strings.Title(strings.ToLower(result)))

}
