package main

import (
	"fmt"
	"math"
)

type Hitung interface {
	luas() float64
	keliling() float64
}

type Segitiga struct {
	Alas   float64
	Tinggi float64
}

type Persegi struct {
	Sisi float64
}

func (p Persegi) luas() float64 {
	return math.Pow(p.Sisi, 2)
}

func (p Persegi) keliling() float64 {
	return p.Sisi * 4
}

func (s Segitiga) luas() float64 {
	return (s.Alas * s.Tinggi) / 2
}

func (s Segitiga) keliling() float64 {
	return s.Alas * 3
}

func main() {
	var bangunDatar Hitung
	var subMenu int
	var inputSisi, inputAlas, inputTinggi float64

	for {

		fmt.Println("=====Sub Menu=====")
		fmt.Println("1. Luas Persegi\n2. Keliling Persegi")
		fmt.Println("3. Luas Segitiga\n4. Keliling Segitiga")
		fmt.Println("0. Keluar")
		fmt.Print("Masukan pilihan sesuai angka yang tersedia : ")
		fmt.Scan(&subMenu)

		if subMenu == 1 {

			fmt.Print("Masukan sisi :")
			fmt.Scan(&inputSisi)
			bangunDatar = Persegi{inputSisi}
			fmt.Println("Luas :", bangunDatar.luas())

		} else if subMenu == 2 {

			fmt.Print("Masukan sisi :")
			fmt.Scan(&inputSisi)
			bangunDatar = Persegi{inputSisi}
			fmt.Println("Keliling :", bangunDatar.keliling())

		} else if subMenu == 3 {

			fmt.Print("Masukan alas :")
			fmt.Scan(&inputAlas)
			fmt.Print("Masukan tinggi :")
			fmt.Scan(&inputTinggi)
			bangunDatar = Segitiga{Alas: inputAlas, Tinggi: inputTinggi}
			fmt.Println("Luas :", bangunDatar.luas())

		} else if subMenu == 4 {

			fmt.Print("Masukan alas :")
			fmt.Scan(&inputAlas)
			fmt.Print("Masukan tinggi :")
			fmt.Scan(&inputTinggi)
			bangunDatar = Segitiga{Alas: inputAlas, Tinggi: inputTinggi}
			fmt.Println("Luas :", bangunDatar.keliling())

		} else if subMenu == 0 {
			break
		} else {
			fmt.Println("tidak ada pilihan")
		}

	}

}
